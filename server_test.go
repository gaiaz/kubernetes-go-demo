package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIndexHandler(t *testing.T) {
	handler := newHandler()
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	handler.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Errorf("expected status code %v but got %v", http.StatusOK, rec.Code)
	}
	if rec.Body.String() != "Hello World" {
		t.Errorf("unexpected body in response: %q", rec.Body.String())
	}
}

func BenchmarkIndexHandler(b *testing.B) {
	handler := newHandler()

	for i := 0; i < b.N; i++ {
		rec := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)

		handler.ServeHTTP(rec, req)

		if rec.Code != http.StatusOK {
			b.Errorf("expected status code %v but got %v", http.StatusOK, rec.Code)
		}
		if rec.Body.String() != "Hello World" {
			b.Errorf("unexpected body in response: %q", rec.Body.String())
		}
	}
}

func TestProbeHandlers(t *testing.T) {
	cases := []struct {
		name string
		path string
	}{
		{
			name: "Readiness",
			path: "/ready",
		},
		{
			name: "Liveness",
			path: "/live",
		},
	}

	handler := newHandler()

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodGet, tc.path, nil)

			handler.ServeHTTP(rec, req)

			if rec.Code != http.StatusOK {
				t.Errorf("expected status code %v but got %v", http.StatusOK, rec.Code)
			}
			if contentType := rec.Header().Get("Content-Type"); jsonContentType != contentType {
				t.Errorf("expected content type %q but got %q", emptyJson, contentType)
			}
			if rec.Body.String() != emptyJson {
				t.Errorf("unexpected body in response: %q", rec.Body.String())
			}
		})
	}
}

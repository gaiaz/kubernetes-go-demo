# Kubernetes Go Demo

[![Build Status](https://gitlab.com/gaiaz/kubernetes-go-demo/badges/master/pipeline.svg)](https://gitlab.com/gaiaz/kubernetes-go-demo/pipelines)

## Usage

```bash
docker run -p 8000:8000 -e PORT=8000 registry.gitlab.com/gaiaz/kubernetes-go-demo:1.0.0
```

## License

[MIT](http://opensource.org/licenses/MIT) © [Gaiaz Iusipov](https://gitlab.com/gaiaz)

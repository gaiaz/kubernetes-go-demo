package main

import (
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"

	"github.com/gorilla/mux"
)

const (
	jsonContentType = "application/json"
	emptyJson       = "{}"
)

func newServer() *http.Server {
	server := &http.Server{
		Addr:    ":" + os.Getenv("PORT"),
		Handler: newHandler(),
	}
	go func() {
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()
	return server
}

func newHandler() *httpHandler {
	router := mux.NewRouter()
	handler := &httpHandler{router: router}
	router.HandleFunc("/", handler.index)
	router.HandleFunc("/ready", handler.ready)
	router.HandleFunc("/live", handler.live)
	router.PathPrefix("/debug/pprof").Handler(http.DefaultServeMux)
	return handler
}

type httpHandler struct {
	router *mux.Router
}

func (s *httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (httpHandler) index(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	_, _ = w.Write([]byte("Hello World"))
}

func (httpHandler) ready(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write([]byte(emptyJson))
}

func (httpHandler) live(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", jsonContentType)
	_, _ = w.Write([]byte(emptyJson))
}
